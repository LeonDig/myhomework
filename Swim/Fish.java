package Swim;
public class Fish implements Swim{
    private String name;

    public Fish(String name) {
        this.name = name;
    }

    @Override
    public void swim() {
        System.out.println("I am a fish "+ name +". I swim by moving my fins.");

    }
}
