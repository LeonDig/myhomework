package Swim;

public class UBoat implements Swim {

    private int speed;

    public UBoat(int speed) {
        this.speed = speed;
    }

    @Override
    public void swim() {
        System.out.println("The submarine sails, spinning the propellers, at a speed is " + speed);
    }
}
