package Swim;
public class Human implements Swim {
    private String name;
    private int age;

    public Human(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public void swim() {
        System.out.println(toString()+" I swim with an inflatable circle.");
    }

    @Override
    public String toString() {
        return "I am " + name + ", my age is " + age  ;
    }

}

