package Test;


import java.time.LocalDate;
import java.util.*;

class Person   {
    int id;
	String name;
    java.time.LocalDate birthdate;




	public java.time.LocalDate getBirthdate() {
		return birthdate;
	}


	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}



	public Person(int age, String name,java.time.LocalDate birthdate ) {
		this.id = age;
		this.name=name;
		this.birthdate = birthdate;
	}
	



	@Override
	public String toString() {
		return "Person{"+ "age= " +id+ " name="+ name+" birthdate= "+birthdate+"}";
	}
}
class ComparePersonId implements Comparator<Person>{
	@Override
	public int compare(Person o1, Person o2) {
		
		return o1.getId()- o2.getId();
	}	
}
class ComparePersonBirthDate implements Comparator<Person>{
	@Override
	public int compare(Person o1, Person o2) {
		
		return o1.getBirthdate().compareTo(o2.getBirthdate());
	}	
}

class ComparePersonName implements Comparator<Person>{
	@Override
	public int compare(Person o1, Person o2) {
		
		return o1.getName().compareTo(o2.getName());
	}	
}

public class comparator {

	public static void main(String[] args) {
		System.out.println("Sort by ID");

		Set set1 = new TreeSet(new ComparePersonId());
		set1.add(new Person (65213 ,"Jhon",LocalDate.of(1962, 3, 12)));
		set1.add(new Person (43115, "Piter",LocalDate.of(1951, 4, 01)));
		set1.add(new Person (45314,"Mike",LocalDate.of(1915, 4, 15)));
		set1.add(new Person (78645,"Bim",LocalDate.of(1945, 6, 05)));
		set1.add(new Person (46312,"Son",LocalDate.of(1953, 3, 15)));
		for(Object o :set1) {
		System.out.println(o);
		}
		
		System.out.println();
		System.out.println("Sort by Name");
		Set set2 = new TreeSet(new ComparePersonName());
		set2.add(new Person (65213 ,"Jhon",LocalDate.of(1962, 3, 12)));
		set2.add(new Person (43115, "Piter",LocalDate.of(1951, 4, 01)));
		set2.add(new Person (45314,"Mike",LocalDate.of(1915, 4, 15)));
		set2.add(new Person (78645,"Bim",LocalDate.of(1945, 6, 05)));
		set2.add(new Person (46312,"Son",LocalDate.of(1953, 3, 15)));
		for(Object o :set2) {
		System.out.println(o);
	}
		System.out.println();
		System.out.println("Sort by Birthday");
		Set set3 = new TreeSet(new ComparePersonBirthDate());
		set3.add(new Person (65213 ,"Jhon",LocalDate.of(1962, 3, 12)));
		set3.add(new Person (43115, "Piter",LocalDate.of(1951, 4, 01)));
		set3.add(new Person (45314,"Mike",LocalDate.of(1915, 4, 15)));
		set3.add(new Person (78645,"Bim",LocalDate.of(1945, 6, 05)));
		set3.add(new Person (46312,"Son",LocalDate.of(1953, 3, 15)));
		for(Object o :set3) {
		System.out.println(o);
	}
	}
}

