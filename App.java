
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


class User{
String name;
int id;
java.time.LocalDate birthdate;




public User(String name, int id, LocalDate birthdate) {
	

	this.name = name;
	this.id = id;
	this.birthdate = birthdate;


}


public int getId() {
	return id;
}


public java.time.LocalDate getBirthdate() {
	return birthdate;
}


public String getName() {
	return name;
}


@Override
public String toString() {
	return "User [name=" + name + ", id=" + id + ",birthdate=" + birthdate + "]";
}



public class App {

	public  void main(String[] args) {


		List<User> userList = new ArrayList<User>();		
		userList.add(new User("John",11111,LocalDate.of(1961, 3, 12)));
		userList.add(new User("Mikle",22222,LocalDate.of(1965, 5, 10)));
		userList.add(new User("Jeck",333333,LocalDate.of(1948, 3, 11)));
		userList.add(new User("Klain",444444,LocalDate.of(1958, 7, 01)));
		userList.add(new User("Jim",55555,LocalDate.of(1966, 5, 30)));

		userList.forEach(std -> System.out.println(std));
		
		
		Collections.sort(userList, new IdCompartor());
		System.out.println("after sort by Id");
		userList.forEach(std -> System.out.println(std));

		
		Collections.sort(userList, new NameCompartor());
		System.out.println("after sort by name");
		userList.forEach(std -> System.out.println(std));

		
		//java 8 comparator
		Comparator<User> cm2 = Comparator.comparing(User::getId);
		Collections.sort(userList, cm2);

		Comparator<User> cm3 = Comparator.comparing(User::getBirthdate);
		Collections.sort(userList, cm3);
	}

}}

